$("#login").click(function () {
	$("#error").hide();
	$("#username, #password").removeClass("error-input");
	$.post({
		url: "/api/login",
		data: {"username": $("#username").val(),"password": $("#password").val()},
		success: function() {
			$(location).attr("href", "/");
		},
		error: function() {
			console.log("Error");
			$("#error").show();
			$("#username, #password").addClass("error-input");
			$("#username, #password").val("");
		},
		dataType: "json"
	});
});