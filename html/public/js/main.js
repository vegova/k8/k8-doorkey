$(".loading").hide();

$("#rfid-add-btn").click(function () {
	$("#spinner1").fadeIn();
	$.get({
		url: "/api/read",
		success: function(data) {
			console.log(data);
			$("#rfid-add").val(data["key"]);
			$("#spinner1").fadeOut();
		},
		error: function(data) {
			console.log("No key");
			$("#spinner1").fadeOut();
		},
		dataType: "json"
	});
});

$("#rfid-remove-btn").click(function () {
	$("#spinner2").fadeIn();
	$.get({
		url: "/api/read",
		success: function(data) {
			console.log(data);
			$("#rfid-remove").val(data["key"]);
			$("#spinner2").fadeOut();
		},
		error: function(data) {
			console.log("No key");
			$("#spinner2").fadeOut();
		},
		dataType: "json"
	});
});

$("#add-btn").click(function () {
	$("#spinner1").fadeIn();
	$("#add-error").hide();
	$("#rfid-add").removeClass("error-input");
	$.post({
		url: "/api/add",
		data: {"username": $("#username").val(),"rfid": $("#rfid-add").val()},
		success: function() {
			console.log("Added");
			$("#rfid-add").val("");
			$("#spinner1").fadeOut();
		},
		error: function() {
			console.log("Error");
			$("#add-error").show();
			$("#rfid-add").addClass("error-input");
			$("#spinner1").fadeOut();
		},
		dataType: "json"
	});
});

$("#remove-btn").click(function () {
	$("#spinner2").fadeIn();
	$("#remove-error").hide();
	$("#rfid-remove").removeClass("error-input");
	$.post({
		url: "/api/remove",
		data: {"rfid": $("#rfid-remove").val()},
		success: function() {
			console.log("Removed");
			$("#rfid-remove").val("");
			$("#spinner2").fadeOut();
		},
		error: function() {
			console.log("Error");
			$("#remove-error").show();
			$("#rfid-remove").addClass("error-input");
			$("#spinner2").fadeOut();
		},
		dataType: "json"
	});
});

$.get({
	url: "/api/log",
	success: function(data) {
		for (let i = 0; i < data.length; i++) {
			let name = data[i]["name"];
			let id = data[i]["id"];
			let rfid = data[i]["rfid"];
			let date = data[i]["time"];
			$("#table > tbody:last-child").append("<tr><td>" + id + "</td><td>" + name + "</td><td>" + rfid + "</td><td>" + date + "</td></tr>");
		}
	},
	error: function(data) {
		console.log("Error");
	},
	dataType: "json"
});
