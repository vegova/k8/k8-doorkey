#!/usr/bin/env python3
import getpass
from lib.database import Connect
from os.path import abspath
from passlib.hash import pbkdf2_sha256

username = input("New username: ")
password = getpass.getpass("New password: ")

db = Connect(abspath("database.db"))
db.change_credentials(username, pbkdf2_sha256.hash(password))
print("\nChange successful!")
