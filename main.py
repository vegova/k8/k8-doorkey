import logging
from lib.reader import Reader
from lib.database import Connect
from lib.ui import Display
from lib.gpio import DoorManager
from lib.server import Management
from os.path import abspath
from time import sleep


class DoorKey(object):
	"""Main Door Key object

	Authors:
		tljubesek
	"""
	def __init__(self):
		logging.basicConfig(filename='log.log', level=logging.INFO)
		self.logger = logging.getLogger(__name__)

		self.rdr = Reader(0x08ff, 0x0009)

		self.db = Connect(abspath("database.db"))

		scene_dir = [abspath("ui/NFC-recognise"), abspath("ui/Door-unlock"), abspath("ui/Door-lock"), abspath("ui/Lock-denied")]
		self.display = Display((800, 1280), scene_dir, abspath("ui/background.png"), abspath("ui/Roboto-Regular.ttf"))
		self.display.play_scene("Door-lock")

		self.mngmt = Management(self.db, self.display, abspath("html"))

		self.door_manager = DoorManager(2, 5)
		self.display.show_text("Ready!")

		self._main()

	def _main(self):
		while True:
			key = self.rdr.read()

			if key:
				if not self.mngmt.reader_in_use():
					data = self.db.get_user(key)
					if data:
						self._access_granted(data)
					else:
						self._access_denied()
				else:
					self.mngmt.send_key(key)

	def _access_granted(self, data):
		self.logger.debug("User: {}".format(data["name"]))

		self.db.add_log(data["rfid"], data["name"])
		self.display.play_scene("Door-unlock")
		self.display.show_text("Welcome, {}".format(data["name"]))
		self.door_manager.open()
		sleep(4)
		self.display.play_scene("Door-lock")

	def _access_denied(self):
		self.logger.info("Tag disabled")
		self.display.play_scene("Lock-denied")
		self.display.show_text("RFID tag not vaild")


if __name__ == '__main__':
	DoorKey()
