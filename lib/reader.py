import usb.core
import time
import threading
import queue
import logging

'''
key codes:
	0x1e = 30 --> 1
	0x1f = 31 --> 2
	0x20 = 32 --> 3
	0x21 = 33 --> 4
	0x22 = 34 --> 5
	0x23 = 35 --> 6
	0x24 = 36 --> 7
	0x25 = 37 --> 8
	0x26 = 38 --> 9
	0x27 = 39 --> 0
	0x28 = 40 --> RETURN
'''

# idVendor=0x08ff, idProduct=0x0009


class Reader(object):
	"""Class to read the output of an RFID scanner that acts like a keyboard

	Authors:
		Jan Leskovec

	Args:
		vendor_id: vendor id of the RFID scanner
		product_id: product id of the RFID scanner
	"""

	def __init__(self, vendor_id, product_id):
		self._buffer = queue.Queue()
		self._scanner = _ReaderThread(vendor_id, product_id, self._buffer)

	def read(self):
		"""A method to be called to get the data from the buffer and clear it

		Authors:
			Jan Leskovec
		"""
		return self._buffer.get()

	def close(self):
		"""A method to be called to release the device before closing the program

		Authors:
			Jan Leskovec
		"""

		self._scanner.kill()
		self._scanner.join()

class _ReaderThread(threading.Thread):
	"""Thread that polls for new data from the USB device

	Authors:
		Jan Leskovec

	Args:
		vendor_id: vendor id of the RFID scanner
		product_id: product id of the RFID scanner
		buffer: a Queue() object that acts as the buffer
	"""


	def __init__(self, vendor_id, product_id, buffer):
		threading.Thread.__init__(self)
		self._logger = logging.getLogger(__name__)

		self._dev = usb.core.find(idVendor=vendor_id, idProduct=product_id)

		self._interface = 0
		self._endpoint = self._dev[0][(0,0)][0]

		self._short_buffer = ""
		self._lastScan = time.time()

		self._buffer = buffer
		self._kill = False

		self.start()

	def kill(self):
		self._kill = True

	def run(self):
		while not self._kill :
			try:
				if self._dev.is_kernel_driver_active(self._interface):
					self._dev.detach_kernel_driver(self._interface)
					usb.util.claim_interface(self._dev, self._interface)

				data = self._dev.read(self._endpoint.bEndpointAddress, self._endpoint.wMaxPacketSize)
				if (time.time() - self._lastScan > 1 and self._short_buffer != ""):
					self._logger.warning("Last scan timed out - _short_buffer cleared")
					self._short_buffer = ""
				self._lastScan = time.time()

				if (data[2] == 0x28):
					self._buffer.put(self._short_buffer)
					self._short_buffer = ""
					self._logger.info("data!")
				else:
					switcher = {
						0x00: "",
						0x1e: "1",
						0x1f: "2",
						0x20: "3",
						0x21: "4",
						0x22: "5",
						0x23: "6",
						0x24: "7",
						0x25: "8",
						0x26: "9",
						0x27: "0",
					}
					input = switcher.get(data[2], None)
					if (input != None):
						self._short_buffer += input
					else:
						self._logger.error("Invalid input!")

			except usb.core.USBError as e:
				data = None
				if e.args == ("Operation timed out",):
					self._logger.error("Operation timed out")
					continue

		usb.util.release_interface(self._dev, self._interface)
		self._dev.attach_kernel_driver(self._interface)
