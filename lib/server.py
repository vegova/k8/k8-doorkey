import cherrypy
import logging
import threading
import os
import glob
import queue
from passlib.hash import pbkdf2_sha256


class Management(object):
	"""Website management object

	Authors:
		tljubesek

	Args:
		database: Database object.
		display: Display object.
		html_path: Relative path to static files.
	"""
	def __init__(self, database, display, html_path):
		self._key_queue = queue.Queue()
		self._reader_active = threading.Event()
		self._server = _ManagementThread(self._reader_active, self._key_queue, database, display, html_path)

	def reader_in_use(self):
		"""Check if web server is using the reader

		Authors:
			tljubesek
		"""
		return self._reader_active.is_set()

	def send_key(self, key):
		"""Send key from the reader to web server

		Authors:
			tljubesek

		Args:
			key: Key to send.
		"""
		self._key_queue.put(key)

	def close(self):
		"""Close the web server thread

		Authors:
			tljubesek
		"""
		self._server.kill()
		self._server.join()


class _ManagementThread(threading.Thread):
	"""Thread that runs the pygame display

	Authors:
		tljubesek

	Args:
		reader_active: Thread event for reader usage status.
		key_queue: Queue object for keys.
		database: Database object.
		display: Display object.
		html_path: Relative path to static files.
	"""
	def __init__(self, reader_active, key_queue, database, display, html_path):
		super(_ManagementThread, self).__init__()
		self._logger = logging.getLogger(__name__)

		self.reader_active = reader_active
		self.key_queue = key_queue
		self.db = database
		self.display = display
		self.html_path = html_path

		self.start()

	def kill(self):
		cherrypy.engine.exit()

	def run(self):
		root_conf = {
		   '/': {
				'tools.sessions.on': True,
				'error_page.default': Root.error_page
			},
			'/public': {
				'tools.staticdir.on': True,
				'tools.staticdir.dir': os.path.abspath(os.path.join(self.html_path, "public"))
			}
		}

		api_conf = {
			'/': {
				'tools.sessions.on': True,
				'error_page.default': Api.error_page
			}
		}

		cherrypy.tree.mount(Root(self.html_path), '/', root_conf)
		cherrypy.tree.mount(Api(self._logger, self.reader_active, self.key_queue, self.db, self.display), '/api', api_conf)
		cherrypy.server.socket_host = "0.0.0.0"
		cherrypy.server.socket_port = 80
		cherrypy.engine.start()


class Root(object):
	"""Root object for web server.

	Authors:
		tljubesek

	Args:
		html_dir: Relative path to static files.
	"""
	def __init__(self, html_dir):
		# Preload html files to RAM
		self.html_dir = html_dir
		self.html_files = {}
		for file in glob.glob(os.path.join(self.html_dir, "*.html")):
			self.html_files[os.path.basename(file)] = open(file, encoding="utf8").read()

	def error_page(status, message, traceback, version):
		return status

	@cherrypy.tools.register("before_handler")
	def require_site_auth():
		if not cherrypy.session.get("login"):
			raise cherrypy.HTTPRedirect("/login")

	@cherrypy.expose
	@cherrypy.tools.require_site_auth()
	def index(self, *vpath, **params):
		return self.html_files["index.html"]

	@cherrypy.expose
	def login(self, *vpath, **params):
		return self.html_files["login.html"]


class Api(object):
	"""Api object for web server.

	Authors:
		tljubesek

	Args:
		logger: Logger object.
		reader_active: Thread event for reader usage status.
		database: Database object.
		display: Display object.
		reader: Reader object.
	"""
	def __init__(self, logger, reader_active, key_queue, database, display):
		self._logger = logger
		self.db = database
		self.display = display
		self.reader_active = reader_active
		self.key_queue = key_queue


	def error_page(status, message, traceback, version):
		return status

	@cherrypy.tools.register("before_handler")
	def require_api_auth():
		if cherrypy.session.get("login") is None:
			raise cherrypy.HTTPError(401, "Unauthorized")

	@cherrypy.expose
	@cherrypy.tools.allow(methods=["POST"])
	def login(self, username, password, *vpath, **params):
		credentials = self.db.get_credentials(username)
		if credentials:
			_, hash_passwrd = credentials
			if pbkdf2_sha256.verify(password, hash_passwrd):
				cherrypy.session["login"] = True
				return "200"
		raise cherrypy.HTTPError(401, "Unauthorized")

	@cherrypy.expose
	@cherrypy.tools.require_site_auth()
	@cherrypy.tools.allow(methods=["POST"])
	def add(self, username, rfid, *vpath, **params):
		if not self.db.get_user(rfid):
			self.db.add_user(username, rfid)
			self._logger.info("Added {} {}".format(username, rfid))
			return "200"
		else:
			raise cherrypy.HTTPError(409, "Conflict")

	@cherrypy.expose
	@cherrypy.tools.require_site_auth()
	@cherrypy.tools.allow(methods=["POST"])
	def remove(self, rfid, *vpath, **params):
		if self.db.get_user(rfid):
			self.db.remove_user(rfid)
			self._logger.info("Removed {}".format(rfid))
			return "200"
		else:
			raise cherrypy.HTTPError(409, "Conflict")

	@cherrypy.expose
	@cherrypy.tools.require_site_auth()
	@cherrypy.tools.json_out()
	@cherrypy.tools.allow(methods=["GET"])
	def log(self, *vpath, **params):
		return self.db.get_log(params.get("rows", 10))

	@cherrypy.expose
	@cherrypy.tools.require_site_auth()
	@cherrypy.tools.json_out()
	@cherrypy.tools.allow(methods=["GET"])
	def read(self, *vpath, **params):
			self.reader_active.set()
			self.display.show_text("Scan RFID card")
			self._logger.info("Reader active")

			try:
				key = self.key_queue.get(timeout=120)
			except queue.Empty as e:
				self._logger.info("No card scanned")
				key = None

			self.reader_active.clear()
			self.display.show_text("RFID: {}".format(key))
			self._logger.info("Reader disabled")
			return {"key": key}


if __name__ == '__main__':
	from database import Connect
	from ui import Display
	from os.path import abspath

	db = Connect(abspath("../database.db"))
	scene_dir = [abspath("../ui/NFC-recognise"), abspath("../ui/Door-unlock"), abspath("../ui/Door-lock"), abspath("../ui/Lock-denied")]
	d = Display((800, 1280), scene_dir, abspath("../ui/background.png"), abspath("../ui/Roboto-Regular.ttf"))
	m = Management(db, d, abspath("../html"))
