import pygame
import glob
import threading
import queue
import os
import logging


class Display(object):
	"""Class for controlling UI

	Authors:
		tljubesek

	Args:
		screen_size: Tuple with width and height of the screen.
		scene_dir_list: List of scene directories.
		background: Background image.
	"""
	def __init__(self, screen_size, scene_dir_list, background, font_file):
		self._play_queue = queue.Queue()
		self._display = _DisplayThread(screen_size, scene_dir_list, background, self._play_queue, font_file)

	def play_scene(self, scene_id):
		"""Add a scene to played on the display

		Authors:
			tljubesek

		Args:
			scene_id: ID of the scene from the scene_dir_list.
		"""
		self._play_queue.put(("scene", scene_id))

	def show_text(self, text):
		"""Add a toast with text to shown on the display

		Authors:
			tljubesek

		Args:
			text: Text in toast.
		"""
		self._play_queue.put(("text", text))

	def close(self):
		self._display.kill()
		self._display.join()


class _DisplayThread(threading.Thread):
	"""Thread that runs the pygame display

	Author:
		tljubesek

	Args:
		screen_size: Tuple with width and height of the screen.
		scene_dir_list: List of scene directories.
		background: Background image.
		play_queue: queue.Queue() object.
		FPS: FPS of the screen.
	"""
	def __init__(self, screen_size, scene_dir_list, background, play_queue, font_file, FPS=30):
		super(_DisplayThread, self).__init__()
		self._play_queue = play_queue
		self._screen_size = screen_size
		self._FPS = FPS
		self._scene_dir_list = scene_dir_list
		self._background = background
		self._font_file = font_file
		self._logger = logging.getLogger(__name__)

		self._logger.info("Running UI")
		self._kill = False
		self.start()

	def kill(self):
		self._kill = True

	def run(self):
		pygame.init()
		pygame.display.set_caption("K8 DoorKey")
		pygame.mouse.set_visible(False)

		surface = pygame.display.set_mode(self._screen_size, pygame.SRCALPHA, 32)

		loadscreen = Loadscreen(surface, 600, 100, len(self._scene_dir_list) + 3)

		self._logger.info("Loading scenes...")
		scenes = {}
		for scene_path in self._scene_dir_list:
			loadscreen.next_load(scene_path)
			self._logger.info("Loading scene: " + scene_path)
			scenes[os.path.basename(scene_path)] = Scene(surface, scene_path, 150, 775)

		self._logger.info("Done loading scenes")
		scene_buffer = []

		loadscreen.next_load("Loading Toast")
		toast_field = Toast(surface, 150, 5, font=self._font_file, font_size=24)
		toast_buffer = []

		loadscreen.next_load(self._background)
		background = pygame.image.load(self._background).convert()
		surface.blit(background, (0, 0))
		pygame.display.flip()

		clock = pygame.time.Clock()
		self._logger.info("Ready!")

		while not self._kill:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					pygame.quit()
					raise SystemExit

			update_rects = []

			if not self._play_queue.empty():
				id, content = self._play_queue.get()
				if id == "scene":
					scene_buffer.append(content)
				elif id == "text":
					toast_buffer.append(content)

			if scene_buffer:
				scenes[scene_buffer[0]].draw()
				update_rects.append(scenes[scene_buffer[0]].rect)

				if not scenes[scene_buffer[0]].running:
					scene_buffer.pop(0)

			if toast_field.running:
				toast_field.draw()
				update_rects.append(toast_field.rect)

				if not toast_field.running:
					toast_buffer.pop(0)

			elif toast_buffer:
				toast_field.update(toast_buffer[0])

			if update_rects:
				pygame.display.update(update_rects)
				surface.blit(background, (0, 0))
			clock.tick(self._FPS)

		pygame.quit()


class Loadscreen(object):
	"""Simple loading screen

	Author:
		tljubesek

	Args:
		surfice: pygame.Surface object to blit to.
		width: Width of the loading screen zone.
		height: Height of the loading screen zone.
		load_num: Number of elements to be loaded.
		font_size: Size of the font (25).
		color: Color of loading screen (255, 255, 255).

	"""
	def __init__(self, surface, width, height, load_num, font_size=25, color=(255,255,255)):
		self.surface = surface
		self.load_num = load_num
		self.num = 0
		self.color = color

		self.bg_surface = pygame.Surface((width, height), pygame.SRCALPHA, 32)
		self.rect = pygame.Rect(0, 0, width, height)
		self.rect.center = surface.get_rect().center

		self.font = pygame.font.Font(None, font_size)

		self.loadbar = pygame.Rect(10, height/3*2, 0, 10)
		self.text_rect = pygame.Rect(0, height/3, 0, font_size)

	def next_load(self, text):
		self.num += 1
		self.loadbar.width = (self.rect.width - 20) / self.load_num * self.num

		text = self.font.render(text, True, self.color)
		self.text_rect.size = text.get_size()
		self.text_rect.centerx = self.rect.width / 2

		self.bg_surface.fill((0,0,0))
		self.bg_surface.blit(text, self.text_rect.topleft)
		pygame.draw.rect(self.bg_surface, self.color, self.loadbar)

		self.surface.blit(self.bg_surface, self.rect.topleft)
		pygame.display.update([self.rect])


class Scene(object):
	"""Helper object for creating and running a scene

	Authors:
		tljubesek

	Args:
		surface: pygame.Surface object to blit scene to.
		frame_dir: Directory of the scene frames.
		x: x top left coordinate.
		y: y top left coordinate.
	"""
	def __init__(self, surface, frame_dir, x, y):
		self.surface = surface

		self.frames = [pygame.image.load(img).convert() for img in sorted(glob.glob(frame_dir + "/*.png"))[::2]] # Load every second frame for 30 fps
		self.frame_count = 0

		self.rect = self.frames[0].get_rect()
		self.rect.topleft = x, y

		self.x, self.y = x, y

		self.running = False

	def draw(self):
		self.running = True
		self.surface.blit(self.frames[self.frame_count], (self.x, self.y))
		self.frame_count += 1

		if self.frame_count >= len(self.frames):
			self.frame_count = 0
			self.running = False


class Toast(object):
	"""Object for creating toast messages

	Authors:
		tljubesek

	Args:
		surface: pygame.Surface object to blit scene to.
		end_height: Toast animation height.
		duration: Duration of the animation.
		speed: Speed of toast ((fast) 0.1 - 1 (slow)).
		font_size: Font size.
		font: Path to font file.
		FPS: Fps of main pygame.Surface object.
	"""
	def __init__(self, surface, end_height, duration, speed=0.2, font_size=14, font=None, FPS=30):
		self.surface = surface
		self._end_height = end_height
		self._speed = speed

		self._delta = (1 / FPS) / duration * 2

		self.padding = int(16 * (font_size / 14))
		self.width = int(344 * (font_size / 14))
		self.height = int(48 * (font_size / 14))

		surface_rect = self.surface.get_rect()
		self.bg_surface = pygame.Surface((self.width, self.height), pygame.SRCALPHA, 32)
		self.rect = pygame.Rect(0, surface_rect.height - self._end_height, self.width, self._end_height)
		self.rect.centerx = surface_rect.centerx

		self.bg_color = (50, 50, 50)
		self.bg_corner_radius = int(4 * (font_size / 14))

		self.font = pygame.font.Font(font, font_size)
		self.font_color = (159, 159, 159)

		self._t = 0
		self.running = False
		self._reverse = False

	def draw(self):
		if self.running:
			if not self._reverse:
				self._t += self._delta
				if self._t >= 1:
					self._reverse = True
			else:
				self._t -= self._delta
				if self._t <= 0:
					self._t = 0
					self._reverse = False
					self.running = False

			# ExponetialOut/In
			y = self._end_height * (-2**(-10 * self._t / self._speed) + 1)
			self.surface.blit(self.bg_surface, (self.rect.x, self.rect.bottom - y))

	def update(self, text):
		"""Helper object for creating text fields

		Authors:
			tljubesek

		Args:
			text: Text to be displayed.
		"""
		pygame.draw.circle(self.bg_surface, self.bg_color, (self.bg_corner_radius, self.bg_corner_radius), self.bg_corner_radius)
		pygame.draw.circle(self.bg_surface, self.bg_color, (self.width - self.bg_corner_radius, self.bg_corner_radius), self.bg_corner_radius)
		pygame.draw.circle(self.bg_surface, self.bg_color, (self.bg_corner_radius, self.height - self.bg_corner_radius), self.bg_corner_radius)
		pygame.draw.circle(self.bg_surface, self.bg_color, (self.width - self.bg_corner_radius, self.height - self.bg_corner_radius), self.bg_corner_radius)
		pygame.draw.rect(self.bg_surface, self.bg_color, (0, self.bg_corner_radius, self.width, self.height - self.bg_corner_radius * 2))
		pygame.draw.rect(self.bg_surface, self.bg_color, (self.bg_corner_radius, 0, self.width - self.bg_corner_radius * 2, self.height))

		txt_img = self.font.render(text, True, self.font_color)
		self.bg_surface.blit(txt_img, (self.padding, self.padding))

		self.running = True


if __name__ == '__main__':
	import time
	scenes = [os.path.abspath("../ui/NFC-recognise"), os.path.abspath("../ui/Door-unlock"), os.path.abspath("../ui/Door-lock"), os.path.abspath("../ui/Lock-denied")]
	d = Display((800, 1280), scenes, os.path.abspath("../ui/background.png"), os.path.abspath("../ui/Roboto-Regular.ttf"))

	d.show_text("Ready!")

	time.sleep(7)
	for n in range(1):
		for i in scenes:
			name = os.path.basename(i)
			d.play_scene(name)
			d.show_text(name)
