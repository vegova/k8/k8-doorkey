import sqlite3
import os


class Connect(object):
	"""Simple mysql database class.

	Authors:
		tljubesek

	Args:
		file_name: File path for SQLite database file.
	"""
	def __init__(self, file_name):
		self.db_file = file_name
		if not os.path.isfile(file_name):
			self._create_users_table()
			self._create_log_table()
			self._create_admin_table()

	def _create_users_table(self):
		db, cursor = self._connect()
		stmt = """CREATE TABLE 'users' (
				'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
				'rfid' TEXT NOT NULL UNIQUE,
				'name' TEXT NOT NULL
				);"""
		cursor.execute(stmt)
		db.commit()
		db.close()

	def _create_log_table(self):
		db, cursor = self._connect()
		stmt = """CREATE TABLE 'log' (
				'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
				'rfid' INTEGER NOT NULL,
				'name' TEXT NOT NULL,
				'time' DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
				);"""
		cursor.execute(stmt)
		db.commit()
		db.close()

	def _create_admin_table(self):
		db, cursor = self._connect()
		stmt = """CREATE TABLE 'admin' (
				'id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
				'username' TEXT,
				'password' TEXT
				);"""
		cursor.execute(stmt)

		stmt = "INSERT INTO admin ('username', 'password') VALUES (NULL, NULL)"
		cursor.execute(stmt)

		db.commit()
		db.close()

	def _connect(self):
		db = sqlite3.connect(self.db_file)
		cursor = db.cursor()
		return db, cursor

	def get_log(self, rows):
		"""Get log from db.

		Authors:
			tljubesek

		Args:
			rows: number of rows to fetch.
		"""
		db, cursor = self._connect()
		stmt = "SELECT id, rfid, name, time FROM log ORDER BY id DESC LIMIT ?"
		cursor.execute(stmt, [rows])
		rows = cursor.fetchall()
		db.close()

		columns = [col[0] for col in cursor.description]
		return [dict(zip(columns, row)) for row in rows]

	def add_log(self, rfid, name):
		"""Add access log to db.

		Authors:
			tljubesek

		Args:
			rifd: RFID used.
			name: users name.

		Returns:
			Dictionary: {{
				"id": 123,
				"rfid": 12345678,
				"name": John Doe,
				"time": <Timestamp>
			}, ...}
		"""
		db, cursor = self._connect()
		stmt = "INSERT INTO log (rfid, name) VALUES (?, ?)"
		cursor.execute(stmt, (rfid, name))
		db.commit()
		db.close()

	def get_user(self, rfid):
		"""Get user info.

		Authors:
			tljubesek

		Args:
			rfid: rfid to check against.

		Returns:
			Dictionary: {
				"id": 123,
				"rfid": 12345678,
				"name": John Doe
			}
			or None if rfid doesn't exist.
		"""
		db, cursor = self._connect()
		stmt = "SELECT id, name FROM users WHERE rfid = ?"
		cursor.execute(stmt, [rfid])
		data = cursor.fetchone()
		db.close()

		if data:
			parsed = {
				"id": int(data[0]),
				"rfid": rfid,
				"name": data[1]
			}
			return parsed
		return None

	def add_user(self, username, rfid):
		"""Add user to db.

		Authors:
			tljubesek

		Args:
			username: Username to add.
			rfid: rfid to add.
		"""
		db, cursor = self._connect()
		stmt = "INSERT INTO users ('rfid', 'name') VALUES (?, ?)"
		cursor.execute(stmt, [rfid, username])
		db.commit()
		db.close()

	def remove_user(self, rfid):
		"""Remove user from db.

		Authors:
			tljubesek

		Args:
			rfid: rfid of user to remove.
		"""
		db, cursor = self._connect()
		stmt = "DELETE FROM users WHERE rfid = ?"
		cursor.execute(stmt, [rfid])
		db.commit()
		db.close()

	def get_credentials(self, username):
		"""Get admin credentials.

		Authors:
			tljubesek

		Args:
			username: Username of admin account.
		"""
		db, cursor = self._connect()
		stmt = "SELECT username, password FROM admin WHERE username = ?"
		cursor.execute(stmt, [username])
		data = cursor.fetchone()
		db.close()
		if data:
			return data[0], data[1]

		return None

	def change_credentials(self, username, password, id=1):
		"""Change admin credentials.

		Authors:
			tljubesek

		Args:
			username: Username to change to.
			password: Password to change to.
			id (optional): Account id.
		"""
		db, cursor = self._connect()
		stmt = "UPDATE admin SET username = ?, password = ? WHERE id = ?"
		cursor.execute(stmt, [username, password, id])
		db.commit()
		db.close()


if __name__ == '__main__':
	db = Connect("../database.db")

	data = db.get_user("0004969478")
	print(data)
	db.add_log(data["id"], data["name"])
