import time
import threading
import RPi.GPIO as gpio


class DoorManager(object):
	"""Class to read the output of an RFID scanner that acts like a keyboard

	Authors:
		Jan Leskovec

	Args:
		pin: the pin which controls the lock
		open_time: the length of time the doors should be open
	"""

	def __init__(self, pin, open_time):
		self.pin = pin
		self.open_time = open_time
		self._thread = _GpioThread(self.pin)

	def open(self):
		"""A method to be called to open the doors

		Authors:
			Jan Leskovec
		"""
		self._thread.time = time.time() + self.open_time

	def close(self):
		self._thread.kill()
		self._thread.join()

class _GpioThread(threading.Thread):
	"""Thread that handles GPIO

	Authors:
		Jan Leskovec

	Args:
		pin: the pin which controls the lock
	"""

	def __init__(self, pin):
		threading.Thread.__init__(self)

		self.pin = pin
		self.time = 0

		gpio.setwarnings(False)

		gpio.setmode(gpio.BCM)
		gpio.setup(self.pin, gpio.OUT)

		self._kill = False

		self.start()

	def kill(self):
		self._kill = True

	def run(self):
		while not self._kill :
			if (time.time() < self.time):
				gpio.output(self.pin, gpio.HIGH)
			else:
				gpio.output(self.pin, gpio.LOW)

			time.sleep(0.1)
		gpio.output(self.pin, gpio.LOW)
