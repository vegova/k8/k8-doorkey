#!/bin/bash

cd "$(dirname "$0")"

cp doorkey.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable doorkey.service

echo "Service copied and enabled. To start it simply reboot or use command: systemctl start doorkey"