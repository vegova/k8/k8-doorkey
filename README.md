<div align="center">
![Logo](https://gitlab.com/lejs/k8-doorkey/raw/master/res/Artboard%2041.png)
</div>

## Features

* Read RFID Tags
* Material Design UI
* User Access Logging
* Web-Based Management Console

[![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://gitlab.com/lejs/k8-doorkey/blob/master/LICENSE)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/Django.svg)](https://www.python.org/downloads/)

## Installation

```
sudo mkdir /opt/doorkey && sudo chown -R pi:pi /opt/doorkey
git clone https://gitlab.com/lejs/k8-doorkey.git /opt/doorkey
sudo apt install python3 python3-pip python3-pygame libusb
sudo pip3 install -r /opt/doorkey/requirements.txt
```

Now change admin credentials with `sudo python3 /opt/doorkey/change_credentials.py`.
This are login credentials for management website.

Finally add execute `sudo /opt/doorkey/setup.sh` to create and enable the systemd service.

You can now restart the pi: `sudo reboot`

Web management console should be now available at devices ip.


## License
All files within this repo are released under the MIT License as per the [LICENSE](https://gitlab.com/lejs/k8-doorkey/blob/master/LICENSE) file stored in the root of this repository.
